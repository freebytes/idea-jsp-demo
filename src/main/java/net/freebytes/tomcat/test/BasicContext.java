package net.freebytes.tomcat.test;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @date: 2021/1/8 15:57
 */
public class BasicContext extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // 通过getServletContext 获取ServletContext 的上下文环境对象
        // getServletContext() 其实是ServletConfig 中的方法
        ServletContext servletContext = getServletContext();
        // ServletContext 的作用范围是整个Web应用，也就是说，你在这个Servlet中设置的值
        // 在其他Servlet中也能够获取到。
        servletContext.setAttribute("name1", "value1");

        // 获取web.xml中定义的属性 <param-name>
        String encode = servletContext.getInitParameter("encode");
        System.out.println("encode = " + encode);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
