package net.freebytes.tomcat.test;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @date: 2021/1/8 15:50
 */
public class TestServletConfig extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletConfig servletConfig = getServletConfig();
        System.out.println(servletConfig.getServletName());
        System.out.println(servletConfig.getInitParameter("key1"));
        System.out.println(servletConfig.getInitParameter("key2"));
    }
}
