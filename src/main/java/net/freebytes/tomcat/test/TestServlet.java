package net.freebytes.tomcat.test;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @date: 2021/1/8 11:34
 */
@WebServlet("/test")
public class TestServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("hello");
        resp.getWriter().write("hello");
    }

    @Override
    public void destroy() {
        System.out.println("---------------------------------------------------------------------");
    }


    @Override
    public void init(ServletConfig config) throws ServletException {
        System.out.println(config);
    }
}
