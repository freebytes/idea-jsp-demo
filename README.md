# 用idea+maven，开发传统JSP项目

#### 介绍
现在的javaWeb开发一般是基于springboot，直接使用idea开发，使用maven管理jar包。但在以前，使用传统的jsp技术的时候，还没有idea这么好用的开发工具，也没有maven。那时候一般是用eclipse，直接下载并导入jar包的方式开发。

日前，想做一下传统tomcat项目相关的研究，就想着搭建一个传统的jsp项目回忆一下老旧的知识。于是试了一下，基于idea和maven，直接搭建jsp项目。

#### 软件架构
软件架构说明


#### 使用说明

查看官方教程  https://www.freebytes.net/it/java/idea-maven-jsp-project.html

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
