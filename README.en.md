# 用idea+maven，开发传统JSP项目

#### Description
现在的javaWeb开发一般是基于springboot，直接使用idea开发，使用maven管理jar包。但在以前，使用传统的jsp技术的时候，还没有idea这么好用的开发工具，也没有maven。那时候一般是用eclipse，直接下载并导入jar包的方式开发。

日前，想做一下传统tomcat项目相关的研究，就想着搭建一个传统的jsp项目回忆一下老旧的知识。于是试了一下，基于idea和maven，直接搭建jsp项目。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
